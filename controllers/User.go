package controllers

import (
	"net/http"
	"odhtest/models"
	"odhtest/utils"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func LoginEndpoints(router *gin.RouterGroup) {
	router.POST("/login", login)
	router.POST("/User/create", createUser)
}

func createUser(c *gin.Context) {
	type RequestData struct {
		UserName string ` json:"user_name"`
		Password string `json:"password"`
		Role     string `json:"role"`
	}
	var requestData RequestData
	// if c.GetHeader("Authorization") == "" {
	// 	c.AbortWithStatusJSON(http.StatusUnauthorized, utils.ErrorMessage(utils.AuthRequiredError, 401))
	// 	return
	// }

	if err := c.BindJSON(&requestData); err != nil {
		c.AbortWithStatusJSON(http.StatusOK, utils.ErrorMessage("Invalid input", http.StatusBadRequest))
		return
	}
	err := models.CreateUser(
		requestData.UserName,
		requestData.Password,
		requestData.Role,
	)
	if err != nil {
		c.AbortWithStatusJSON(200, utils.ErrorMessage("invalid ", http.StatusInternalServerError))
		return
	}
	title := "User"
	description := "create user successful"
	c.AbortWithStatusJSON(http.StatusOK, utils.SuccessMessage(utils.DataObject{
		Title:       &title,
		Description: &description,
	}))
}

func login(c *gin.Context) {
	var user models.User
	if err := c.BindJSON(&user); err != nil {
		c.AbortWithStatusJSON(http.StatusOK, utils.ErrorMessage("Invalid input", http.StatusBadRequest))
		return
	}

	var user1 models.User
	utils.SQLDB.Table("users").Select("*").Where("user_name = ?", user.UserName).Find(&user1)

	if user1.UserName == "" {
		c.JSON(http.StatusOK, utils.ErrorMessage("username or password is incorrect", 200))
		return
	} else if err := bcrypt.CompareHashAndPassword([]byte(user1.Password), []byte(user.Password)); err != nil {
		return
	} else {
		Token := models.Jwwt(&user1)
		user1.Token = Token
		models.UpdateToken(user1)
		title := "Login Succesful"
		userdetail, _ := models.GetEmployee(user1.UserName)
		c.AbortWithStatusJSON(http.StatusOK, utils.SuccessMessage(utils.DataObject{
			Title: &title,
			User:  userdetail,
			Token: &Token,
		}))
	}
	return
}
