package controllers

import (
	"net/http"
	"odhtest/models"
	"odhtest/utils"

	"github.com/gin-gonic/gin"
)

func EmployeeEndpoints(router *gin.RouterGroup) {
	router.POST("create", createEmployee)
	router.PATCH("update", updateEmployee)
	router.DELETE("delete", deleteEmployee)
	router.GET("get", getAllEmployees)
	router.GET("getone", getEmployee)
}

func createEmployee(c *gin.Context) {
	type RequestData struct {
		Name     string ` json:"name"`
		LastName string ` json:"last_name"`
		Position string ` json:"position"`
		BirthD   string ` json:"birth_d"`
		Age      uint32 ` json:"age"`
		Sex      string ` json:"sex"`
		Email    string ` json:"email"`
		Phone    string `json:"phone"`
		UserName string `json:"user_name"`
	}
	var requestData RequestData

	if err := c.BindJSON(&requestData); err != nil {
		c.AbortWithStatusJSON(http.StatusOK, utils.ErrorMessage("Invalid input", http.StatusBadRequest))
		return
	}
	role := models.CheckRole(c.GetHeader("Authorization"))
	if role != "admin" {
		c.AbortWithStatusJSON(http.StatusOK, utils.ErrorMessage("admin only", http.StatusBadRequest))
		return
	}
	err := models.CreateEmployee(
		requestData.Name,
		requestData.LastName,
		requestData.Position,
		requestData.BirthD,
		requestData.Age,
		requestData.Sex,
		requestData.Email,
		requestData.Phone,
		requestData.UserName,
	)
	if err != nil {
		c.AbortWithStatusJSON(200, utils.ErrorMessage("invalid ", http.StatusInternalServerError))
		return
	}
	title := "Employee"
	description := "create employee successful"
	c.AbortWithStatusJSON(http.StatusOK, utils.SuccessMessage(utils.DataObject{
		Title:       &title,
		Description: &description,
	}))
}

func updateEmployee(c *gin.Context) {
	var employee *models.Employee
	if e := c.BindJSON(&employee); e != nil {
		c.AbortWithStatusJSON(http.StatusOK, utils.ErrorMessage("Invalid input", http.StatusBadRequest))
		return
	}
	role := models.CheckRole(c.GetHeader("Authorization"))
	if role != "admin" {
		c.AbortWithStatusJSON(http.StatusOK, utils.ErrorMessage("admin only", http.StatusBadRequest))
		return
	}
	err := models.UpdateEmployee(employee)
	if err != nil {
		c.AbortWithStatusJSON(200, utils.ErrorMessage("invalid input ", http.StatusInternalServerError))
		return
	}
	title := "Employee"
	description := "update employee successful"
	c.AbortWithStatusJSON(http.StatusOK, utils.SuccessMessage(utils.DataObject{
		Title:       &title,
		Description: &description,
	}))

}

func deleteEmployee(c *gin.Context) {
	type RequestData struct {
		UserName string `json:"user_name"`
	}

	var requestData RequestData
	if e := c.BindJSON(&requestData); e != nil {
		c.AbortWithStatusJSON(http.StatusOK, utils.ErrorMessage("Invalid input", http.StatusBadRequest))
		return
	}
	role := models.CheckRole(c.GetHeader("Authorization"))
	if role != "admin" {
		c.AbortWithStatusJSON(http.StatusOK, utils.ErrorMessage("admin only", http.StatusBadRequest))
		return
	}
	err := models.DeleteEmployee(
		requestData.UserName)

	if err != nil {
		c.AbortWithStatusJSON(200, utils.ErrorMessage("Can not Delete ", http.StatusInternalServerError))
		return
	}
	title := "Delete employee successful"

	c.AbortWithStatusJSON(http.StatusOK, utils.SuccessMessage(utils.DataObject{
		Title: &title,
	}))

}

func getAllEmployees(c *gin.Context) {
	results, err := models.GetEmployees()
	role := models.CheckRole(c.GetHeader("Authorization"))
	if role != "admin" {
		c.AbortWithStatusJSON(http.StatusOK, utils.ErrorMessage("admin only", http.StatusBadRequest))
		return
	}

	if err != nil {
		c.AbortWithStatusJSON(404, utils.ErrorMessage("Employees not fount", http.StatusExpectationFailed))
		return
	}
	type ResponseData struct {
		Employees interface{} `json:"employees"`
	}

	responseData := ResponseData{
		Employees: results,
	}
	title := "Employee"
	description := "Get employees successful"
	c.AbortWithStatusJSON(http.StatusOK, utils.SuccessMessage(utils.DataObject{
		Title:       &title,
		Description: &description,
		User:        responseData,
	}))

}

func getEmployee(c *gin.Context) {
	type RequestData struct {
		UserName string `json:"user_name"`
	}

	var requestData RequestData
	//fmt.Println(c.GetHeader("Authorization"))
	if e := c.BindJSON(&requestData); e != nil {
		c.AbortWithStatusJSON(http.StatusOK, utils.ErrorMessage("Invalid input", http.StatusBadRequest))
		return
	}
	role := models.CheckRole(c.GetHeader("Authorization"))
	if role != "admin" {
		c.AbortWithStatusJSON(http.StatusOK, utils.ErrorMessage("admin only", http.StatusBadRequest))
		return
	}
	results, err := models.GetEmployee(requestData.UserName)
	if err != nil {
		c.AbortWithStatusJSON(404, utils.ErrorMessage("Employees not fount", http.StatusExpectationFailed))
		return
	}
	type ResponseData struct {
		Employees interface{} `json:"employees"`
	}

	responseData := ResponseData{
		Employees: results,
	}
	title := "Employee"
	description := "Get employees successful"
	c.AbortWithStatusJSON(http.StatusOK, utils.SuccessMessage(utils.DataObject{
		Title:       &title,
		Description: &description,
		User:        responseData,
	}))

}
