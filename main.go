package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"odhtest/controllers"
	middlewares "odhtest/middlewares"
	"odhtest/models"
	"odhtest/utils"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.Use(middlewares.CORSMiddleware())
	r.NoRoute(func(c *gin.Context) {
		c.JSON(http.StatusNotFound, utils.ErrorMessage("endpoint not found", 404))
	})
	r.NoMethod(func(c *gin.Context) {
		c.JSON(http.StatusMethodNotAllowed, utils.ErrorMessage("method not allow", 405))
	})
	utils.InitialDB()
	models.DoautoMigration(utils.SQLDB)
	controllers.EmployeeEndpoints(r.Group("/employee"))
	controllers.LoginEndpoints(r.Group(""))
	targetPort, isSet := os.LookupEnv("PORT")
	if len(targetPort) == 0 || !isSet {
		targetPort = "8081"
	}
	srv := &http.Server{
		Addr:    ":" + targetPort,
		Handler: r,
	}

	go func() {
		fmt.Println("Running on Port: ", targetPort)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutting down server...")
	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}

	log.Println("Server exiting")

}
