FROM golang:alpine AS builder

ENV PROJECT_PATH=/api-menu-d
WORKDIR $PROJECT_PATH

# ARG buildenv=dev
COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix nocgo -o /server .

FROM scratch
COPY --from=builder ./server /
COPY ./configs ./configs



EXPOSE 8081

ENTRYPOINT ["./server"] 