package middlewares

import (
	"github.com/gin-gonic/gin"
)

// func AuthMiddleware() gin.HandlerFunc {
// 	return func(c *gin.Context) {
// 		if c.GetHeader("Authorization") == "" {
// 			c.AbortWithStatusJSON(http.StatusUnauthorized, utils.ErrorMessage(utils.AuthRequiredError, 401))
// 			return
// 		}
// 		claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
// 			Issuer:    strconv.Itoa(int(user.Id)),
// 			ExpiresAt: time.Now().Add(time.Hour * 24).Unix(), // 1 day
// 		})

// 		token, err := claims.SignedString([]byte("secret"))

// 		if err != nil {
// 			return c.SendStatus(fiber.StatusInternalServerError)

// 		}

// 	}
// }
// func AuthMiddleware(Token string) bool {
// 	claims := jwt.MapClaims{}
// 	token, err := jwt.ParseWithClaims(Token, claims, func(token *jwt.Token) (interface{}, error) {
// 		return []byte("<YOUR VERIFICATION KEY>"), nil
// 	})
// 	if claims == "admin"{

// 	}
// 	return
// }

// func AuthMiddleWare() gin.HandlerFunc {
// 	return func(c *gin.Context) {
// 		if c.GetHeader("Authorization") == "" {
// 			c.AbortWithStatusJSON(http.StatusUnauthorized, utils.ErrorMessage(utils.AuthRequiredError, 401))
// 			return
// 		}
// 		fmt.Println(c.GetHeader("Authorization"))
// 		claims := jwt.MapClaims{}
// 		token, _ := jwt.ParseWithClaims(c.GetHeader("Authorization"), claims, func(token *jwt.Token) (interface{}, error) {
// 			for key, val := range claims {
// 				fmt.Printf("Key: %v, value: %v\n", key, val)
// 			}
// 			return claims, nil

// 		})
// 		a, _ := token.SignedString([]byte("secret"))
// 		fmt.Println("asdad ?", a)

// 	}
// }

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Add("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, UPDATE, PATCH")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(200)
		} else {
			c.Next()
		}
	}
}
