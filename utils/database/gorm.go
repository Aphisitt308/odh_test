package database

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	_ "github.com/denisenkom/go-mssqldb"
	"github.com/tidwall/gjson"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
)

type databaseConfiguration struct {
	Server   string
	Port     int
	User     string
	Password string
	Database string
}

// var SQL *sql.DB

// func NewSQLDatabase() (*sql.DB, error) {

// 	var err error
// 	var target string = "Dev"
// 	body, _ := ioutil.ReadFile("./configs/json/sql_database.json")
// 	configValue := gjson.Get(string(body), target)
// 	currentConfig := databaseConfiguration{}
// 	json.Unmarshal([]byte(configValue.String()), &currentConfig)

// 	connectionString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s;",
// 		currentConfig.Server,
// 		currentConfig.User,
// 		currentConfig.Password,
// 		currentConfig.Port,
// 		currentConfig.Database,
// 	)
// 	SQL, err = sql.Open("sqlserver", connectionString)
// 	if err != nil {
// 		fmt.Println(err.Error())
// 		panic(err)
// 	}
// 	if err != nil {
// 		log.Fatal(err)
// 		return nil, err
// 	}

// 	return SQL, nil
// }

var SQL *gorm.DB

func NewSQLDatabase() (*gorm.DB, error) {

	var err error
	var target string = "Dev"
	body, _ := ioutil.ReadFile("./configs/json/sql_database.json")
	configValue := gjson.Get(string(body), target)
	currentConfig := databaseConfiguration{}
	json.Unmarshal([]byte(configValue.String()), &currentConfig)

	connectionString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s;",
		currentConfig.Server,
		currentConfig.User,
		currentConfig.Password,
		currentConfig.Port,
		currentConfig.Database,
	)
	// qq, err := sql.Open("sqlserver", connectionString)
	// SQL, err := gorm.Open(mysql.New(mysql.Config{
	// 	Conn: qq}), &gorm.Config{})
	SQL, err := gorm.Open(sqlserver.Open(connectionString), &gorm.Config{})
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	return SQL, nil
}
