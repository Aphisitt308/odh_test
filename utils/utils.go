package utils

import (
	"odhtest/utils/database"

	"gorm.io/gorm"
)

var SQLDB *gorm.DB

func InitialDB() error {
	if SQLDB == nil {
		DB, err := database.NewSQLDatabase()
		if err != nil {
			return err
		}
		SQLDB = DB
	}
	return nil
}
