package models

import (
	"gorm.io/gorm"
)

func DoautoMigration(dbSrc *gorm.DB) {
	dbSrc.AutoMigrate(&Employee{})
	dbSrc.AutoMigrate(&User{})
}
