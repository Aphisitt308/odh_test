package models

import (
	"odhtest/utils"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	UserName string `gorm:"NOT NULL" json:"user_name"`
	Password string `gorm:"NOT NULL" json:"password"`
	Role     string `gorm:"NOT NULL" json:"role"`
	Token    string `gorm:"" json:"token"`
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}
func CreateUser(UserName string, Password string, Role string) error {

	hash, _ := HashPassword(Password)
	var createUser = User{
		UserName: UserName,
		Password: hash,
		Role:     Role,
	}
	utils.SQLDB.Create(&createUser)
	return nil

}
func Jwwt(user *User) string {
	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Issuer:    user.Role,
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(), // 1 day
	})

	token, _ := claims.SignedString([]byte("secret"))
	return token
}

func UpdateToken(User1 User) error {
	// utils.SQLDB.Exec("UPDATE employees SET (?) WHERE id = ?", Employeeup, Employeeup.ID)
	utils.SQLDB.Model(User{}).Where(`user_name = ?`, User1.UserName).Update("token", User1.Token)
	return nil
}

func CheckRole(Token string) string {

	Token1 := strings.Split(Token, "Bearer ")
	Token2 := strings.Join(Token1, "")
	type user struct {
		Role string ` json:"role"`
	}
	var user1 user
	utils.SQLDB.Table("users").Select("role").Where("token = ?", Token2).Find(&user1)

	return user1.Role
}
