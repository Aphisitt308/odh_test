package models

import "odhtest/utils"

type Employee struct {
	// ID       uint32 `gorm:"primary_key;index;AUTO_INCREMENT" json:"id"`
	Name     string `gorm:"NOT NULL" json:"name"`
	LastName string `gorm:"NOT NULL" json:"last_name"`
	Position string `gorm:"NOT NULL" json:"position"`
	BirthD   string `gorm:"NOT NULL" json:"birth_d"`
	Age      uint32 `gorm:"NOT NULL" json:"age"`
	Sex      string `gorm:"NOT NULL" json:"sex"`
	Email    string `gorm:"NOT NULL" json:"email"`
	Phone    string `gorm:"NOT NULL" json:"phone"`
	UserName string `gorm:"" json:"user_name"`
	// User     User   `gorm:"foreignKey:UserName;OnUpdate:RESTRICT,OnDelete:RESTRICT"`
}

func CreateEmployee(Name string, LastName string, Position string, BirthD string,
	Age uint32, Sex string, Email string, Phone string, UserName string) error {
	var createEmployee = Employee{
		Name:     Name,
		LastName: LastName,
		Position: Position,
		BirthD:   BirthD,
		Age:      Age,
		Sex:      Sex,
		Email:    Email,
		Phone:    Phone,
		UserName: UserName,
	}
	utils.SQLDB.Create(&createEmployee)
	return nil
}

func DeleteEmployee(UserName string) error {
	//var err error
	var employee Employee
	var user User
	// err = utils.SQLDB.Begin().
	// 	Joins("JOIN users ON users.user_name = employees.user_name ").
	// 	Where("employees.user_name = ?", UserName).Find(&employee).Error
	// if err != nil {
	// 	utils.SQLDB.Begin().Rollback()
	// 	return err
	// }
	// err = utils.SQLDB.Begin().
	// 	Joins("JOIN employees ON users.user_name = employees.user_name ").
	// 	Where("employees.user_name = ?", UserName).Find(&user).Error
	// if err != nil {
	// 	utils.SQLDB.Begin().Rollback()
	// 	return err
	// }
	//utils.SQLDB.Table("employees").Delete("")
	// err = utils.SQLDB.Begin().Exec(`SET FOREIGN_KEY_CHECKS=0`).Error
	// if err != nil {
	// 	utils.SQLDB.Begin().Rollback()
	// 	return err
	// }
	utils.SQLDB.Where(`users.user_name = ?`, UserName).Delete(&user)
	utils.SQLDB.Where(`employees.user_name = ?`, UserName).Delete(&employee)
	return nil
}

func UpdateEmployee(Employeeup *Employee) error {
	// utils.SQLDB.Exec("UPDATE employees SET (?) WHERE id = ?", Employeeup, Employeeup.ID)
	utils.SQLDB.Model(Employee{}).Where(`user_name = ?`, Employeeup.UserName).Updates(Employeeup)
	return nil
}

func GetEmployees() (interface{}, error) {
	type SQLEmployee struct {
		Name     string ` json:"name"`
		LastName string ` json:"last_name"`
		Position string ` json:"position"`
		BirthD   string ` json:"birth_d"`
		Age      uint32 ` json:"age"`
		Sex      string ` json:"sex"`
		Email    string ` json:"email"`
		Phone    string `json:"phone"`
		UserName string `json:"user_name"`
	}
	var sqlEmployee []SQLEmployee
	//utils.SQLDB.Query("SELECT * FROM employees", &sqlEmployee)
	// if err := myQuery.Scan(&sqlEmployee).Error; err != nil {
	// 	return nil,myQuery.Err()
	// }
	myQuery := utils.SQLDB.Table("employees").Select("*")
	if err := myQuery.Find(&sqlEmployee).Error; err != nil {
		return nil, err
	}
	return sqlEmployee, nil
}

func GetEmployee(UserName string) (interface{}, error) {
	type SQLEmployee struct {
		Name     string ` json:"name"`
		LastName string ` json:"last_name"`
		Position string ` json:"position"`
		BirthD   string ` json:"birth_d"`
		Age      uint32 ` json:"age"`
		Sex      string ` json:"sex"`
		Email    string ` json:"email"`
		Phone    string `json:"phone"`
		UserName string `json:"user_name"`
	}
	var sqlEmployee []SQLEmployee
	//utils.SQLDB.Query("SELECT * FROM employees WHERE id = ?", ID, &sqlEmployee)
	myQuery := utils.SQLDB.Table("employees").Select("*").Where("user_name = ?", UserName)
	if err := myQuery.Find(&sqlEmployee).Error; err != nil {
		return nil, err
	}
	return sqlEmployee, nil
}
